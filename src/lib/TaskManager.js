let taskList = [];

class TaskManager {
  addTask(task) {
    taskList.unshift(task);
  }

  deleteTask(taskId) {
    taskList = taskList.filter((task) => task.id !== taskId);
  }

  updateTask(taskData) {
    let index = taskList.findIndex((task) => task.id === taskData.id);
    taskList[index] = taskData;
    console.log(taskList);
  }

  getTaskById(taskId) {
    return taskList.filter((task) => task.id === taskId)[0];
  }

  getTasks() {
    return taskList;
  }
}

export default new TaskManager();

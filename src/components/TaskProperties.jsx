import { Col, Row, Tooltip, Button } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import React from "react";

const TaskProperties = ({ task, handleDeleteTask, handleTaskUpdate }) => {
  const handleDeletion = () => {
    handleDeleteTask(task.id);
  };

  const handleUpdate = () => {
    handleTaskUpdate(task.id);
  };

  return (
    <>
      <Col style={{ margin: "8px 16px", padding: "8px 0" }}>
        <h2 style={{ color: "#919191" }}>Properties</h2>
        <Row justify="space-between">
          <h3>Actions</h3>
          <section>
            <Tooltip title="Edit task">
              <Button
                type="dashed"
                icon={<EditOutlined />}
                onClick={handleUpdate}
              >
                Edit
              </Button>
            </Tooltip>
            <Tooltip title="Delete task">
              <Button
                danger
                shape="circle"
                icon={<DeleteOutlined />}
                style={{ marginLeft: "8px" }}
                onClick={() => handleDeletion()}
              />
            </Tooltip>
          </section>
        </Row>
        <section>
          <span>
            <strong>task ID: </strong>
            {task.id}
          </span>
        </section>
      </Col>
    </>
  );
};

export default TaskProperties;

import { Col, Row } from "antd";
import React from "react";

const TaskDetails = ({ task }) => {
  return (
    <>
      <Col style={{ margin: "8px", padding: "8px 8px" }}>
        <h2 style={{ color: "#919191" }}>Task Details</h2>
        <Row justify="space-between">
          <h3>
            <strong>{task.title}</strong>
          </h3>
          <span>
            <strong>Due: </strong>
            {task.dueDate._d.toDateString()}
          </span>
        </Row>

        <p>{task.description}</p>
        <Row></Row>
      </Col>
    </>
  );
};

export default TaskDetails;

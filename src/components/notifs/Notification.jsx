import { notification } from "antd";

class Notification {
  successOperation = (message, description) => {
    notification.success({
      message,
      placement: "topRight",
      description,
    });
  };

  errorOperation = (message, description) => {
    notification.error({
      message,
      description,
      placement: "topRight",
    });
  };
}
export default new Notification();

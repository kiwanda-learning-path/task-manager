import { message } from "antd";

class Message {
  success = (msg) => message.success(msg);

  error = (msg) => message.error(msg);

  warning = (msg) => message.warning(msg);

  info = (msg) => message.info(msg);
}

export default new Message();

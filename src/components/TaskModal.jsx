import React, { useEffect, useState } from "react";
import { Modal, Input, Col, DatePicker, Row } from "antd";
import TextArea from "antd/lib/input/TextArea";
import TaskManager from "../lib/TaskManager";
import Notification from "./notifs/Notification";

const TaskModal = ({
  updateMode,
  isVisible,
  task,
  handleModalCancel,
  handleSave,
}) => {
  const [newTask, setNewTask] = useState({
    id: 0,
    title: "",
    dueDate: "",
    description: "",
  });

  useEffect(() => {
    if (updateMode && task !== null) {
      setNewTask(TaskManager.getTaskById(task.id));
    }
  }, [updateMode, task]);

  function handleDateChange(date, dateString) {
    setNewTask({ ...newTask, dueDate: date });
  }

  function handleTitleChange(evt) {
    setNewTask({ ...newTask, title: evt.target.value });
  }

  function handleDescriptionChange(evt) {
    setNewTask({ ...newTask, description: evt.target.value });
  }

  function saveTask() {
    if (!newTask.title || !newTask.description || !newTask.dueDate) {
      Notification.errorOperation(
        "Error Data",
        "Please fill all inputs of this form with expected data!"
      );
    } else {
      // Proceed saving
      handleSave(newTask, updateMode);
      // Hide modal
      handleModalCancel();
      // Empty form inputs
      setNewTask({
        id: 0,
        title: "",
        dueDate: "",
        description: "",
      });
    }
  }

  return (
    <>
      <Modal
        title="New Task"
        visible={isVisible}
        onOk={() => saveTask()}
        onCancel={() => handleModalCancel()}
        okText={updateMode ? "Update" : "Create"}
        cancelText="Cancel"
      >
        <Col>
          <Row>
            <Col
              lg={{ span: 17 }}
              xs={{ span: 24 }}
              style={{ marginRight: "16px" }}
            >
              <Input
                placeholder="Task title"
                value={newTask.title}
                onChange={handleTitleChange}
              />
            </Col>
            <Col lg={{ span: 6 }} xs={{ span: 24 }}>
              <DatePicker
                placeholder="Due date"
                value={newTask.dueDate}
                onChange={handleDateChange}
              />
            </Col>
          </Row>
          <TextArea
            rows={4}
            placeholder="Task description goes here"
            value={newTask.description}
            style={{ margin: "4px 0" }}
            onChange={handleDescriptionChange}
          />
        </Col>
      </Modal>
    </>
  );
};

export default TaskModal;

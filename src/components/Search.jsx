import { Input } from "antd";
import React from "react";

const Search = ({ term, handleSearchKeyword, sRef }) => (
  <Input.Search
    ref={sRef}
    type="text"
    placeholder="Search using keyword"
    defaultValue={term}
    onChange={handleSearchKeyword}
    style={{
      width: "100%",
    }}
  />
);

export default Search;

import React from "react";
import { List, Avatar } from "antd";
import InfoBadge from "./InfoBadge";
import "./taskList.css";

const TaskList = ({ taskArray, handSelection, titleText }) => {
  function handleTaskSelection(task) {
    handSelection(task);
  }

  return (
    <>
      <h2 style={{ color: "#919191" }}>
        {<InfoBadge value={taskArray.length} />} {titleText}
      </h2>
      <List
        itemLayout="horizontal"
        dataSource={taskArray}
        renderItem={(task) => (
          <List.Item
            className="task-item"
            onClick={() => handleTaskSelection(task)}
          >
            <List.Item.Meta
              avatar={
                <Avatar style={{ backgroundColor: "#698c91" }} size="large">
                  {task.title.charAt(0)}
                  {task.title.split(" ")[1]
                    ? task.title.split(" ")[1].charAt(0)
                    : ""}
                </Avatar>
              }
              title={<span>{task.title}</span>}
              description={task.description}
            />
          </List.Item>
        )}
      />
    </>
  );
};

export default TaskList;

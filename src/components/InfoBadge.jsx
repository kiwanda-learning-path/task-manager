import React from "react";
import { Badge } from "antd";

const InfoBadge = ({ value }) => (
  <Badge
    className="site-badge-count-109"
    count={value}
    style={{ backgroundColor: "rgb(60, 102, 240)" }}
  />
);

export default InfoBadge;

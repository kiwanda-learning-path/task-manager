import { Col, Row, Button, Empty } from "antd";
import Title from "antd/lib/typography/Title";
import "./App.css";
import { PlusOutlined } from "@ant-design/icons";
import TaskDetails from "./components/TaskDetails";
import TaskList from "./components/TaskList";
import TaskProperties from "./components/TaskProperties";
import React, { useEffect, useState, useRef } from "react";
import TaskModal from "./components/TaskModal";
import TaskManager from "./lib/TaskManager";
import Message from "./components/notifs/Message";
import Search from "./components/Search";

function App() {
  const [isModalVisible, setModalVisibility] = useState(false);

  const showModalComponent = () => setModalVisibility(true);
  const hideModalComponent = () => setModalVisibility(false);

  const [taskArray, setTaskArray] = useState([]);
  const [currentTask, setCurrentTask] = useState(null);
  const [isUpdateMode, setIsUpdateMode] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResultList, setSearchResultList] = useState([]);

  const searchRef = useRef("");

  const loadData = () => {
    setTaskArray(TaskManager.getTasks());
  };

  useEffect(() => {
    loadData();
  }, []);

  function handleSelectedTask(selectedTask) {
    setCurrentTask(selectedTask);
  }

  function addNewTask(task, updateMode) {
    if (updateMode) {
      TaskManager.updateTask(task);
      setCurrentTask(null);
      loadData();
      Message.success("Task updated");
    } else {
      TaskManager.addTask({ ...task, id: taskArray.length + 1 }, updateMode);
      Message.info("New task added");
    }
  }

  function removeTask(taskId) {
    TaskManager.deleteTask(taskId);
    setCurrentTask(null);
    loadData();
    Message.info("Task removed successfully");
  }

  const updateTask = (taskId) => {
    setIsUpdateMode(true);
    showModalComponent();
  };

  const showModalForNewTask = () => {
    setIsUpdateMode(false);
    setCurrentTask(null);
    showModalComponent();
  };

  function handleSearch() {
    setSearchTerm(searchRef.current.input.value);
    if (searchTerm !== "") {
      const newTaskArray = taskArray.filter((task) => {
        return Object.values(task)
          .join(" ")
          .toLowerCase()
          .includes(searchTerm.toLowerCase());
      });
      setSearchResultList(newTaskArray);
    } else {
      setSearchResultList(taskArray);
    }
  }

  return (
    <>
      <div className="app">
        <Col>
          <Row justify="center" align="middle">
            <Col
              xs={{ span: 24 }}
              lg={{ span: 22 }}
              xxl={{ span: 18 }}
              className="main"
            >
              <h1>
                Kiwanda | <Title className="tm">Task Manager</Title>
              </h1>
            </Col>
          </Row>
          <Col xs={{ span: 24 }} lg={{ span: 18 }}>
            <Row justify="center" align="middle">
              <Col span={8}>
                <Button
                  onClick={() => showModalForNewTask()}
                  type="primary"
                  shape="round"
                  ghost
                  icon={<PlusOutlined />}
                  size="large"
                  style={{ margin: "16px 0" }}
                >
                  New task
                </Button>
              </Col>
              {taskArray.length > 1 && (
                <Col span={10}>
                  <Search
                    sRef={searchRef}
                    term={searchTerm}
                    handleSearchKeyword={handleSearch}
                  />
                </Col>
              )}
            </Row>
          </Col>
          {taskArray.length !== 0 ? (
            <Row justify="center" style={{ padding: "8px" }}>
              <Col
                xs={{ span: 24 }}
                lg={{ span: 6 }}
                xxl={{ span: 6 }}
                style={{ backgroundColor: "#e9f7f7", paddingLeft: "8px" }}
              >
                <TaskList
                  titleText={
                    searchTerm.length > 0 ? "Element match(es)" : "Recent tasks"
                  }
                  taskArray={
                    searchTerm.length < 1 ? taskArray : searchResultList
                  }
                  handSelection={handleSelectedTask}
                />
              </Col>
              {currentTask && (
                <>
                  <Col xs={{ span: 24 }} lg={{ span: 8 }} xxl={{ span: 6 }}>
                    <TaskDetails task={currentTask} />
                  </Col>
                  <Col
                    xs={{ span: 24 }}
                    lg={{ span: 6 }}
                    xxl={{ span: 4 }}
                    style={{ backgroundColor: "#f7f7f7" }}
                  >
                    <TaskProperties
                      handleDeleteTask={removeTask}
                      handleTaskUpdate={updateTask}
                      task={currentTask}
                    />
                  </Col>
                </>
              )}
            </Row>
          ) : (
            <Col span={24}>
              <Row justify="center">
                <Col>
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                  <h2 style={{ color: "#919191" }}>
                    {" "}
                    There is no pending task, add one +
                  </h2>
                </Col>
              </Row>
            </Col>
          )}
        </Col>
        <TaskModal
          task={currentTask}
          updateMode={isUpdateMode}
          isVisible={isModalVisible}
          handleModalCancel={() => hideModalComponent()}
          handleSave={addNewTask}
        />
      </div>
    </>
  );
}

export default App;

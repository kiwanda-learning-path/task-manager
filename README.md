# Kiwanda: React Experts 🚀  | Task Manager

Simple task manager application performing basic CRUD operations on task. Deployed on firebase [here](https://kiwanda-task-manager-leoka.web.app/) as part of `ReactJs Experts` by [kiwanda](https://kiwanda.org/)

## Technologies

* React v17
* antd
* antd icons
* Firebase (hosting)

## Topics covered

* React Hooks
* Event handler
* Parent - child component communication
* Usage of third party library (antd) for nice UIs
* Component reusability, etc.

## To be improved

* Restriction on task due date (should be future date)
* Add reminder feature (should be nice 😻)

## Credit
Happy coding 🔥 